note
	description: "animals application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit

	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			a: ANIMAL
			c: COW
			g: GRASS
			f: FOOD
		do
			create c
			create g
			a := c
			f := g
			print (a.out + " is going to eat: " + f.out + "%N")
			a.eat (f)
		end

end
